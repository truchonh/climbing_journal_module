const express = require('express');
const mongoose = require('mongoose');

const ClimbingJournalCtrl = require('./api/controllers/climbingJournalCtrl');

/**
 * @param {object} options
 * @param {string} options.mongodb_host
 * @returns {Router}
 */
module.exports = function _registerRoutes(options) {
    mongoose.Promise = global.Promise;
    mongoose
        .connect(options.mongodb_host, { useNewUrlParser: true })
        .catch(err => console.error('Error loading database: ' + err));

    const router = express.Router({});

    router.use('/climbing', ClimbingJournalCtrl.getRouter());

    return router;
};